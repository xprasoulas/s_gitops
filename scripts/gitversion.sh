#!/bin/bash

echo "running git version and setting version in pipeline variables using dotenv artifacts"

/tools/dotnet-gitversion /output buildserver $VERBOSITYSETTING

grep 'GitVersion_LegacySemVer='          gitversion.properties >> thisversion.env
grep 'GitVersion_SemVer='          gitversion.properties >> thisversion.env
grep 'GitVersion_FullSemVer='      gitversion.properties >> thisversion.env
grep 'GitVersion_Major='           gitversion.properties >> thisversion.env
grep 'GitVersion_Minor='           gitversion.properties >> thisversion.env
grep 'GitVersion_Patch='           gitversion.properties >> thisversion.env
grep 'GitVersion_MajorMinorPatch=' gitversion.properties >> thisversion.env
